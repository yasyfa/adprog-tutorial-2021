package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {

    @Override
    public String getType() {
        return "ranged";
    }

    @Override
    public String attack() {
        return "Jedar! pala ilang!";
    }
}
